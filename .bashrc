#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias vi='vim'
#PS1='[\u@\h \W]\$ '

[ -n "$PS1" ] && source ~/.bash_prompt

# To apply wal (pywal) to new terminals
(cat ~/.cache/wal/sequences &)


source /usr/share/fzf/key-bindings.bash
source /usr/share/fzf/completion.bash
