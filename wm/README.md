## Requirements
You first have to install a bunch of packages in order to make it work.

> If you're running low hardware you should not use picom. Be sure to remove corresponding entry into `i3/config`

- `i3-gaps`
- [i3lock-fancy-rapid](https://github.com/yvbbrjdr/i3lock-fancy-rapid)
- `termite`
- [polybar](https://github.com/polybar/polybar) (compile it from sources)
- `feh`
- `pywal` for generating colors based on your wallpaper
- [picom](https://github.com/sdhand/picom) for transparency/shadows with round corners
- `awesome-terminal-fonts` for polybar icons

### Installation
For `i3lock-fancy` and `polybar` you'll have to compile them from their respective sources by cloning sources then use `make` command.
After that you might have to put the generated file to `/usr/bin/`.
-> [Some extra info for polybar](https://github.com/polybar/polybar/wiki/Compiling)

After that, copy all this stuff into `$HOME/.config`

> gtk-3.0 folder is aimed to add some padding into your terminal

## Preview
![preview](preview.png)
