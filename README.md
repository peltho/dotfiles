## Dotfiles
Here is a list of mandatory dotfiles in order to have a decent working environment.

> Beware that dotfiles are hidden by default given they start with a dot `.` !

### Additional info
For `vim` be sure to install [vim-plug](https://github.com/junegunn/vim-plug) before running `:PlugInstall`
Plus you need to install a powerline font for `vim-airline`.
